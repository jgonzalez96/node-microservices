const { default: axios } = require('axios');
const express = require('express');

const app = express();
app.use(express.json());

const events = [];

app.post('/events', async (req, res) => {
  const event = req.body;

  events.push(event);

  axios.post('http://posts-clusterip-srv:4000/events', event).catch(console.log);
  axios.post('http://comments-srv:4001/events', event).catch(console.log);
  axios.post('http://query-srv:4002/events', event).catch(console.log);
  axios.post('http://moderation-srv:4003/events', event).catch(console.log);

  res.send({ status: 'OK' });
});

app.get('/events', async (req, res) => {
  res.send(events);
});

app.listen(4005, () => {
  console.log('Listening on port 4005');
});
